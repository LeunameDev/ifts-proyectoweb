CREATE DATABASE IF NOT EXISTS SGTDB;
USE SGTDB;

CREATE TABLE Usuario (
	`id_Usuario` INT AUTO_INCREMENT,
    `matricula_Usuario` INT NOT NULL,
    `nombre_Usuario` VARCHAR(70) NOT NULL,
    `apellido_Usuario` VARCHAR(70) NOT NULL,
    `correo` VARCHAR(255) NOT NULL,
    `contraseña` VARCHAR(30) NOT NULL,
    `correo_Recuperacion` VARCHAR(255) NOT NULL,
    `rol` BIT NOT NULL,
    PRIMARY KEY(`id_Usuario`, `matricula_Usuario`, `nombre_Usuario`, `correo`)
);

CREATE TABLE Materia (
	`id_Materia` INT PRIMARY KEY AUTO_INCREMENT,
    `nombre_Materia` VARCHAR(255),
    `horario_Cursada` DATETIME,
    `profesor_Asignado` VARCHAR(70),
    `correlatividad` BIT,
    `modalidad` BIT,
    `cuatrimestre` VARCHAR(100),
    `año` DATE
);

CREATE TABLE Materia_Materia (
	`id_Materia_Materia` INT PRIMARY KEY AUTO_INCREMENT,
    `id_Materia` INT,
    CONSTRAINT `fk_Materia_Materia1_idx` FOREIGN KEY(`id_Materia`) REFERENCES `Materia`(`id_Materia`)
);

CREATE TABLE Usuario_Materia (
	`id_usuario_Materia` INT PRIMARY KEY AUTO_INCREMENT,
    `id_Usuario` INT,
    `matricula_Usuario` INT,
    `nombre_Usuario` VARCHAR(70) NOT NULL,
    `correo` VARCHAR(255) NOT NULL,
    `id_Materia` INT,
    CONSTRAINT `fk_Usuario_Materia_Materia1_idx` FOREIGN KEY(`id_Materia`) REFERENCES `Materia`(`id_Materia`),
    CONSTRAINT `fk_Usuario_Materia_Usuario1_idx` FOREIGN KEY(`id_Usuario`, `matricula_Usuario`, `nombre_Usuario`, `correo`) REFERENCES `Usuario`(`id_Usuario`, `matricula_Usuario`, `nombre_Usuario`, `correo`)
);

CREATE TABLE Carrera (
	`id_Carrera` INT PRIMARY KEY AUTO_INCREMENT,
    `nombre_Carrera` VARCHAR(255),
    `turno_Carrera` VARCHAR(255)
);

CREATE TABLE Usuario_Materia_Carrera (
	`id_usuario_Materia_Carrera` INT PRIMARY KEY AUTO_INCREMENT,
    `id_usuario_Materia` INT,
    `id_Carrera` INT,
    CONSTRAINT `fk_Usuario_Materia_Carrera_Carrera1_idx` FOREIGN KEY(`id_Carrera`) REFERENCES `Carrera`(`id_Carrera`),
    CONSTRAINT `fk_Usuario_Materia_Usuario_Materia1_idx` FOREIGN KEY(`id_usuario_Materia`) REFERENCES `Usuario_Materia`(`id_usuario_Materia`)
);