# Desarrollo de aplicaciones web
## 💻 Proyecto - Sitio web para IFTS

### Tecnologias utilizadas en Front-end
- **Metodologia BEM (CSS)**
- **Variables CSS (No es una tecnologia, más bien son propiedades personalizadas)**
- **HTML5**
- **CSS3**
- **Flexbox**

------------

### Tecnologias próximas a implementar en Back-end & Front-end
- **PHP (Lenguaje de programación)**
- **MySQL (Gestor de bases de datos)**
- **Javascript**